Opis projektu:
Sieciowy serwer chatu

Serwer sieciowy korzystający z protokołu TCP/IP umożliwiający użytkownikom prowadzenie rozmów.

Cechy projektu:
-wielowątkowość - zapewnia większą wydajność
-zapisywanie dziennika wydarzeń

Przy pracy nad projektem korzystać będę z książki
"Unix Network Programming, Volume 1: The Sockets Networking API (Third Edition)" autorstwa:
W. Richard Stevens, Bill Fenner, Andrew M.Rudoff
