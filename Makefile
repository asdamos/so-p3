CC=g++
CFLAGS=-pthread
SOURCES=main.cpp unp.hpp functions.cpp safe_queue.hpp wrapper_functions.cpp log_thread.hpp log_thread.cpp working_thread.hpp working_thread.cpp
EXECUTABLE=Server
PORT=1050

program:
	$(CC) $(CFLAGS) $(SOURCES) -o $(EXECUTABLE)

run:
	./$(EXECUTABLE) $(PORT)

clean:
	rm *o $(EXECUTABLE)
