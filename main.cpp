#include "unp.hpp"
#include <cstdlib>

vector <Client> client_database; //vector for client database

Client_sock conn_clients[FD_SETSIZE];
int maxi = -1;          //max index of client[] array
fd_set      allset;

pthread_mutex_t clients_mutex;

squeue <Received_message> received_messages; //queue for received messages to work on
squeue <string> log_queue; //queue to store log messages

#include "log_thread.hpp"
#include "working_thread.hpp"

using namespace std;

int SERV_PORT=15000;


//MAIN THREAD - HANDLING CONNECTIONS AND RECEIVING MESSAGES
int main(int argc, char **argv)
{
    if(argc != 2)
    {
        //err_sys("Wrong argument number");
    }
    else
    {
        SERV_PORT = atoi(argv[1]);
    }

    pthread_t log_thread, working_thread;
    pthread_mutex_init(&clients_mutex, NULL);

    Pthread_create(&log_thread, NULL, log_thread_function, NULL);           //THREAD TO SAVE LOG
    Pthread_create(&working_thread, NULL, working_thread_function, NULL);   //THREAD TO WORK ON RECEIVED MESSAGES

    int         i, maxfd;
    int			listenfd, connfd, sockfd;   //listen socket and connected socket
    int         nready;
    ssize_t     n;
    fd_set      rset;
    char	    buff[MAXLINE], buff_log[MAXLINE];
    socklen_t   clilen;

    struct sockaddr_in	cliaddr, servaddr;

    listenfd = Socket(AF_INET, SOCK_STREAM, 0);

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(SERV_PORT);


    Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));

    Listen(listenfd, LISTENQ);

    maxfd = listenfd;   //initialize

    string log = "Server started on port ";
    sprintf(buff_log, "%d", SERV_PORT);
    log += buff_log;
    send_log_message(log);

    load_clients();
    send_log_message("Database loaded");

    for(i = 0; i < FD_SETSIZE; i++) //initializing client[] array to -1
    {
        conn_clients[i].sock = -1;
        conn_clients[i].conn_status = 0;
        conn_clients[i].chatroom = 0;
        conn_clients[i].cl = NULL;
    }

    FD_ZERO(&allset);   //clear all entries in set
    FD_SET(listenfd, &allset); //add listenfd to allset

    while(true)
    {
        rset = allset;		/* structure assignment */
        nready = Select(maxfd+1, &rset, NULL, NULL, NULL);

        if (FD_ISSET(listenfd, &rset))  	/* new client connection */
        {
            clilen = sizeof(cliaddr);
            connfd = Accept(listenfd, (SA *) &cliaddr, &clilen);


            for (i = 0; i < FD_SETSIZE; i++)
                if (conn_clients[i].sock < 0)
                {
                    log = "New user connected on socket ";
                    sprintf(buff_log, "%d", connfd);
                    log += buff_log;
                    send_log_message(log);


                    pthread_mutex_lock(&clients_mutex);
                    conn_clients[i].sock = connfd;	/* save descriptor */


                    Received_message msg;
                    memset(msg.message, '\0', MAXLINE);
                    msg.cl_sock = &conn_clients[i];
                    msg.information = 1;
                    strcpy(msg.message, "LOGIN/REGISTER\n");
                    received_messages.push(msg);

                    pthread_mutex_unlock(&clients_mutex);

                    break;
                }

            if (i == FD_SETSIZE)
                err_sys("too many clients");

            FD_SET(connfd, &allset);	/* add new descriptor to set */
            if (connfd > maxfd)
                maxfd = connfd;			/* for select */
            if (i > maxi)
                maxi = i;				/* max index in client[] array */

            if (--nready <= 0)
                continue;				/* no more readable descriptors */
        }

        for (i = 0; i <= maxi; i++)  	/* check all clients for data */
        {
            if ( (sockfd = conn_clients[i].sock) < 0)
                continue;
            if (FD_ISSET(sockfd, &rset))
            {
                memset(buff, '\0', MAXLINE);
                if ( (n = Read(sockfd, buff, MAXLINE)) == 0)
                {
                    /*connection closed by client */
                    log = "User: ";
                    sprintf(buff_log, "%d", sockfd);
                    log += buff_log;
                    log += " disconnected";
                    send_log_message(log);

                    pthread_mutex_lock(&clients_mutex);

                    conn_clients[i].conn_status = 0;
                    conn_clients[i].chatroom = 0;
                    memset(conn_clients[i].temp_nick, '\0', MAXLINE);
                    if(conn_clients[i].cl != NULL)
                    {
                        conn_clients[i].cl->logged = false;
                        conn_clients[i].cl = NULL;

                    }
                    conn_clients[i].sock = -1;
                    Close(sockfd);
                    FD_CLR(sockfd, &allset);
                    pthread_mutex_unlock(&clients_mutex);
                }
                else
                {
                    /*received messages*/
                    log = "User ";
                    sprintf(buff_log, "%d", sockfd);
                    log += buff_log;
                    log += ": ";
                    log += buff;
                    send_log_message(log);

                    Received_message msg;
                    memset(msg.message, '\0', MAXLINE);
                    msg.cl_sock = &conn_clients[i];
                    strcpy(msg.message, buff);
                    msg.information = 0;
                    received_messages.push(msg);

                }
                if (--nready <= 0)
                    break;				/* no more readable descriptors */
            }
        }
    }
}

