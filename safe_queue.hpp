#ifndef __safe_queue_hpp
#define __safe_queue_hpp

/*safe for threads queue class template using stl list*/
#include <pthread.h>
#include <list>
using namespace std;

template <typename T> class squeue
{
private:
    list<T> my_queue;
    pthread_mutex_t mutex;
    pthread_cond_t cond;

public:
    squeue()
    {
        pthread_mutex_init(&mutex, NULL);
        pthread_cond_init(&cond, NULL);
    };

    ~squeue()
    {
        pthread_mutex_destroy(&mutex);
        pthread_cond_destroy(&cond);
    };

    void push(T item)
    {
        pthread_mutex_lock(&mutex);
        my_queue.push_back(item);
        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex);
    };

    T pop()
    {
        pthread_mutex_lock(&mutex);
        while(my_queue.size() == 0)
        {
            pthread_cond_wait(&cond, &mutex);
        }
        T item = my_queue.front();
        my_queue.pop_front();
        pthread_mutex_unlock(&mutex);
        return item;
    };

    int size()
    {
        pthread_mutex_lock(&mutex);
        int qsize = my_queue.size();
        pthread_mutex_unlock(&mutex);
        return qsize;
    };

};


#endif
