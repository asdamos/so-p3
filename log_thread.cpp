#include "log_thread.hpp"

void * log_thread_function(void *arg)
{
    char filename[MAXLINE];
    strcpy(filename, createLogFilename().c_str());
    FILE *log = fopen(filename, "a");
    if (log == NULL)
    {
        err_sys("Could not create log file");
    }
    fclose(log);

    while(true)
    {
        string log_message;
        log_message = log_queue.pop();
        log = fopen(filename, "a");
        fprintf(log, "%s\n", log_message.c_str());
        fclose(log);
    }
    return 0;
}


