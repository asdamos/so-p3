#include "unp.hpp"


string currentDateTime()
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buff[80];
    tstruct = *localtime(&now);
    strftime(buff, sizeof(buff), "[%d-%m-%Y.%X] ", &tstruct);

    return buff;
}


string createLogFilename()
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buff[80];
    tstruct = *localtime(&now);
    strftime(buff, sizeof(buff), "logs/%d_%m_%Y_%H_%M_%S.log", &tstruct);

    return buff;
}

void send_log_message(string message)
{
    string buff = currentDateTime() + message;
    log_queue.push(buff);
}

//function to load clients from database
void load_clients()
{
    FILE *database = fopen("database.txt", "r");

    if (database == NULL)
    {
        err_sys("Could not open database file");
    }

    char nick[MAXLINE], password[MAXLINE];
    int type;
    while(fscanf(database, "%s %s %d", nick, password, &type) != EOF)
    {
        Client client_buff;
        client_buff.account_type = type;
        strcpy(client_buff.nickname, nick);
        strcpy(client_buff.password, password);
        client_buff.logged = false;

        client_database.push_back(client_buff);
    }
    fclose(database);
}


void save_registered_user()
{
    FILE *database = fopen("database.txt", "a");

    if (database == NULL)
    {
        err_sys("Could not open database file");
    }

    client_database[client_database.size()-1];


    fprintf(database, "%s %s %d\n",client_database[client_database.size()-1].nickname,
            client_database[client_database.size()-1].password,
            client_database[client_database.size()-1].account_type);

    fclose(database);
}


void check_command(Received_message msg)
{
    int len = strlen(msg.message);

    msg.message[len-1] = '\0';
    msg.message[len-2] = '\0';

    vector<string> words;

    if(len - 3> 0)  //user typed something else than dot
    {
        //getting single words
        char * pch;
        pch = strtok (msg.message," ,.-$");
        while (pch != NULL)
        {
            char buff[MAXLINE];
            sprintf(buff, "%s", pch);
            words.push_back(string(buff));
            pch = strtok (NULL, " ,.-$");
        }

        if(words.size() > 0)
        {
            int command = 0;
            string log;
            char buff_log[MAXLINE];

            if(words[0].compare(string("disconnect")) == 0)
            {
                command = 1;
            }
            else if(words[0].compare(string("chatroom")) == 0)
            {
                command = 2;
            }
            else if(words[0].compare(string("changeroom")) == 0)
            {
                command = 3;
            }
            else if(words[0].compare(string("kick")) == 0)
            {
                command = 4;
            }

            switch(command)
            {
            case 1://disconnect
                Writen(msg.cl_sock->sock, (void *) "Disconnected\n" , strlen("Disconnected\n"));
                log = "User ";
                sprintf(buff_log, "%d", msg.cl_sock->sock);
                log += buff_log;
                log += " disconnected";
                send_log_message(log);

                msg.cl_sock->conn_status = 0;
                msg.cl_sock->chatroom = 0;
                memset(msg.cl_sock->temp_nick, '\0', MAXLINE);
                if(msg.cl_sock->cl != NULL)
                {
                    msg.cl_sock->cl->logged = false;
                    msg.cl_sock->cl = NULL;
                }

                shutdown(msg.cl_sock->sock, SHUT_RDWR);
                Close(msg.cl_sock->sock);
                FD_CLR(msg.cl_sock->sock, &allset);
                //Writen(msg.cl_sock->sock, (void *) "\n" , strlen("\n"));
                msg.cl_sock->sock = -1;
                break;

            case 2://chatroom number
                char buff[MAXLINE];
                sprintf(buff, "You are in chatroom number: %d\n", msg.cl_sock->chatroom);
                Writen(msg.cl_sock->sock, (void *) buff , strlen(buff));
                break;

            case 3://change chatroom
                if(words.size() == 2)
                {
                    int nchatnumber = atoi(words[1].c_str());
                    msg.cl_sock->chatroom = nchatnumber;

                    char buff[MAXLINE];
                    sprintf(buff, "You entered chatroom number: %d\n", msg.cl_sock->chatroom);
                    Writen(msg.cl_sock->sock, (void *) buff , strlen(buff));

                    log = "User ";
                    sprintf(buff_log, "%d", msg.cl_sock->sock);
                    log += buff_log;
                    log += " changed room into ";
                    sprintf(buff_log, "%d\n", msg.cl_sock->chatroom);
                    log += buff_log;
                    send_log_message(log);

                }
                else
                {
                    Writen(msg.cl_sock->sock, (void *) "Wrong argument number\n" , strlen("Wrong argument number\n"));
                }
                break;
            case 4:
                if(words.size() == 2)
                {
                    if(msg.cl_sock->cl->account_type == 1)  //is admin
                    {
                        for(int i = 0; i<=maxi; i++)
                        {
                            if((conn_clients[i].conn_status == 5) && ((words[1].compare(string(conn_clients[i].cl->nickname)) == 0) ))
                            {
                                Writen(conn_clients[i].sock, (void *) "You have been kicked\n" , strlen("You have been kicked\n"));
                                log = "User ";
                                sprintf(buff_log, "%d", conn_clients[i].sock);
                                log += buff_log;
                                log += " kicked";
                                send_log_message(log);

                                conn_clients[i].conn_status = 0;
                                conn_clients[i].chatroom = 0;
                                memset(conn_clients[i].temp_nick, '\0', MAXLINE);
                                if(conn_clients[i].cl != NULL)
                                {
                                    conn_clients[i].cl->logged = false;
                                    conn_clients[i].cl = NULL;
                                }

                                shutdown(conn_clients[i].sock, SHUT_RDWR);
                                Close(conn_clients[i].sock);
                                FD_CLR(conn_clients[i].sock, &allset);
                                //Writen(msg.cl_sock->sock, (void *) "\n" , strlen("\n"));
                                conn_clients[i].sock = -1;
                                break;
                            }
                        }
                    }
                    else
                    {
                        Writen(msg.cl_sock->sock, (void *) "Command not allowed for you account type\n" , strlen("Command not allowed for you account type\n"));
                    }
                }
                break;
            default:
                Writen(msg.cl_sock->sock, (void *) "Unknown command\n" , strlen("Unknown command\n"));
                break;
            }
        }
    }



    else//user only typed dot
    {
        Writen(msg.cl_sock->sock, (void *) "Unknown command\n" , strlen("Unknown command\n"));
    }
}
