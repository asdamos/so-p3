/* Our header file */

#ifndef __unp_hpp
#define __unp_hpp

#include <iostream>
#include <sys/socket.h>
#include <sys/types.h>
#include <cstdlib>
#include <strings.h>
#include <netinet/in.h>	/* sockaddr_in{} and other Internet defns */

#include "time.h"
#include <sys/socket.h>
#include <string.h>
#include <cstdio>
#include <pthread.h>
#include <unistd.h>
#include <locale>

#include <semaphore.h>

#include <stdarg.h>

#include <errno.h>

#include <vector>
#include <queue>
#include <string>

#include "safe_queue.hpp"

using namespace std;


#define	SA	struct sockaddr

#define	LISTENQ		1024	/* 2nd argument to listen() */
#define	MAXLINE		4096	/* max text line length */
#define	MAXSOCKADDR  128	/* max socket address structure size */
#define	BUFFSIZE	8192	/* buffer size for reads and writes */


void err_sys(const char* x); /* error function */

int		 Accept(int, SA *, socklen_t *);
void	 Bind(int, const SA *, socklen_t);
void	 Connect(int, const SA *, socklen_t);
void	 Listen(int, int);
int		 Socket(int, int, int);

void	 Close(int);
void	 Write(int, void *, size_t);


string currentDateTime();

string createLogFilename();

void send_log_message(string message);

void save_registered_user();


void Pthread_create(pthread_t *id, const pthread_attr_t *attr, void* (*fun)(void*), void* arg);

int Select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);

ssize_t Read(int fd, void *ptr, size_t nbytes);

ssize_t writen(int fd, const void *vptr, size_t n); /* Write "n" bytes to a descriptor. */

void Writen(int fd, void *ptr, size_t nbytes);



//struct for one client
struct Client
{
    char nickname[MAXLINE];
    char password[MAXLINE];
    int account_type; //0 - user, 1 - admin
    bool logged;
};

//struct for socket + pointer on client + some other things that have been found to be usefull
struct Client_sock
{
    int chatroom;
    int conn_status;  //0 if user not connected, 1 LOGIN-nickname, 2 REGISTER-nickname, 3 LOGIN-password, 4 REGISTER-password, 5 logged in
    int sock;
    char temp_nick[MAXLINE];
    Client *cl;
};

struct Received_message
{
    char message[MAXLINE];
    Client_sock *cl_sock;
    int n;
    int information;
};


void load_clients();
void check_command(Received_message msg);

extern vector <Client> client_database; //vector for connected clients
extern int maxi;
extern pthread_mutex_t clients_mutex;
extern fd_set   allset;
extern Client_sock conn_clients[FD_SETSIZE];
extern squeue <Received_message> received_messages; //queue for received messages to work on
extern squeue <string> log_queue; //queue to store log messages
#endif
