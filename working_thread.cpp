#include "working_thread.hpp"

void * working_thread_function(void *arg)
{
    while(true)
    {
        Received_message msg;
        msg = received_messages.pop();


        pthread_mutex_lock(&clients_mutex);

        if(msg.information == 1)
        {
            Writen(msg.cl_sock->sock, msg.message , strlen(msg.message));
        }
        else
        {

            int len = strlen(msg.message);

            switch(msg.cl_sock->conn_status)
            {
            case 5:
                if(msg.message[0] == '$')//user probably typed command
                {
                    check_command(msg);
                }
                else
                {
                    char message_buff[MAXLINE];
                    memset(message_buff, '\0', MAXLINE);
                    sprintf(message_buff, "%s: %s", msg.cl_sock->cl->nickname, msg.message);

                    for(int i=0; i<=maxi; i++)
                    {
                        if(( conn_clients[i].sock != msg.cl_sock->sock ) && ( conn_clients[i].sock != -1 ) && (conn_clients[i].conn_status == 5) && (conn_clients[i].chatroom == msg.cl_sock->chatroom))
                        {
                            Writen(conn_clients[i].sock, message_buff, strlen(message_buff));
                        }
                    }
                }
                break;

            case 4://register needs password
                //Remove \n and \r
                msg.message[len-1] = '\0';
                msg.message[len-2] = '\0';

                if(len-2 > 0)
                {
                    char c;
                    locale loc;
                    bool ok = 1;
                    for(int i=0; i<len-2; i++)
                    {
                        c = msg.message[i];
                        if(isspace(c, loc))
                        {
                            ok = 0;
                            Writen(msg.cl_sock->sock, (void *) "Please do not use whitespaces\n", strlen("Please do not use whitespaces\n"));
                            Writen(msg.cl_sock->sock, (void *) "PASSWORD\n" , strlen("PASSWORD\n"));
                            break;
                        }
                    }

                    if(ok)
                    {

                        Writen(msg.cl_sock->sock, (void *) "Registered and connected\n" , strlen("Registered and connected\n"));

                        Client registered;
                        registered.account_type = 0;
                        registered.logged = true;
                        strcpy(registered.nickname, msg.cl_sock->temp_nick);
                        strcpy(registered.password, msg.message);

                        client_database.push_back(registered);

                        msg.cl_sock->cl = &client_database[client_database.size()-1];   //pointer to last added element
                        save_registered_user();

                        string log = "User ";
                        char buff_log[MAXLINE];
                        sprintf(buff_log, "%d", msg.cl_sock->sock);
                        log += buff_log;
                        log += " registered as ";
                        log +=msg.cl_sock->cl->nickname;
                        send_log_message(log);
                        msg.cl_sock->conn_status = 5;
                    }
                    else
                    {
                        Writen(msg.cl_sock->sock, (void *) "PASSWORD\n" , strlen("PASSWORD\n"));
                    }
                }
                break;

                break;
            case 3://login needs password
                //Remove \n and \r
                msg.message[len-1] = '\0';
                msg.message[len-2] = '\0';

                if(len-2 > 0)
                {
                    char c;
                    locale loc;
                    bool ok = 1;
                    for(int i=0; i<len-2; i++)
                    {
                        c = msg.message[i];
                        if(isspace(c, loc))
                        {
                            ok = 0;
                            Writen(msg.cl_sock->sock, (void *) "Please do not use whitespaces\n", strlen("Please do not use whitespaces\n"));
                            Writen(msg.cl_sock->sock, (void *) "PASSWORD\n" , strlen("PASSWORD\n"));
                            break;
                        }
                    }

                    if(ok)
                    {
                        if(strcmp(msg.message, msg.cl_sock->cl->password) == 0)//password correct
                        {
                            Writen(msg.cl_sock->sock, (void *) "Connected\n" , strlen("Connected\n"));
                            msg.cl_sock->conn_status = 5;
                            msg.cl_sock->cl->logged = true;
                            string log = "User ";
                            char buff_log[MAXLINE];
                            sprintf(buff_log, "%d", msg.cl_sock->sock);
                            log += buff_log;
                            log += " logged in as ";
                            log +=msg.cl_sock->cl->nickname;
                            send_log_message(log);
                        }
                        else
                        {
                            Writen(msg.cl_sock->sock, (void *) "Password does not match\n" , strlen("Password does not match\n"));
                            Writen(msg.cl_sock->sock, (void *) "PASSWORD\n" , strlen("PASSWORD\n"));
                        }
                    }
                    else
                    {
                        Writen(msg.cl_sock->sock, (void *) "PASSWORD\n" , strlen("PASSWORD\n"));
                    }
                }
                break;

            case 2://typed REGISTER
                //Remove \n and \r
                msg.message[len-1] = '\0';
                msg.message[len-2] = '\0';

                if(len-2 > 0)
                {
                    char c;
                    locale loc;
                    bool ok = 1;
                    for(int i=0; i<len-2; i++)
                    {
                        c = msg.message[i];
                        if(isspace(c, loc))
                        {
                            ok = 0;
                            Writen(msg.cl_sock->sock, (void *) "Please do not use whitespaces\n", strlen("Please do not use whitespaces\n"));
                            Writen(msg.cl_sock->sock, (void *) "NICKNAME\n" , strlen("NICKNAME\n"));
                            break;
                        }
                    }

                    if(ok)
                    {
                        bool found = false;
                        //we have to find that nickname in database
                        for(unsigned int i=0; i<client_database.size(); i++)
                        {
                            if(strcmp(msg.message, client_database[i].nickname) == 0)//we found your username
                            {
                                Writen(msg.cl_sock->sock, (void *) "User already registered\n" , strlen("User already registered\n"));
                                Writen(msg.cl_sock->sock, (void *) "NICKNAME\n" , strlen("NICKNAME\n"));

                                found = true;
                                break;
                            }
                        }
                        if(!found)
                        {
                            strcpy(msg.cl_sock->temp_nick, msg.message);
                            Writen(msg.cl_sock->sock, (void *) "PASSWORD\n" , strlen("PASSWORD\n"));
                            msg.cl_sock->conn_status = 4;
                        }
                    }
                }
                else
                {
                    Writen(msg.cl_sock->sock, (void *) "NICKNAME\n" , strlen("NICKNAME\n"));
                }
                break;

            case 1://typed LOGIN
                //Remove \n and \r
                msg.message[len-1] = '\0';
                msg.message[len-2] = '\0';

                if(len-2 > 0)
                {
                    char c;
                    locale loc;
                    bool ok = 1;
                    for(int i=0; i<len-2; i++)
                    {
                        c = msg.message[i];
                        if(isspace(c, loc))
                        {
                            ok = 0;
                            Writen(msg.cl_sock->sock, (void *) "Please do not use whitespaces\n", strlen("Please do not use whitespaces\n"));
                            Writen(msg.cl_sock->sock, (void *) "NICKNAME\n" , strlen("NICKNAME\n"));
                            break;
                        }
                    }

                    if(ok)
                    {
                        bool found = false;
                        //we have to find that nickname in database
                        for(unsigned int i=0; i<client_database.size(); i++)
                        {
                            if(strcmp(msg.message, client_database[i].nickname) == 0)//we found your username
                            {
                                if(client_database[i].logged)
                                {
                                    Writen(msg.cl_sock->sock, (void *) "User already connected\n" , strlen("User already connected\n"));
                                    Writen(msg.cl_sock->sock, (void *) "NICKNAME\n" , strlen("NICKNAME\n"));
                                }
                                else
                                {
                                    msg.cl_sock->conn_status = 3;
                                    msg.cl_sock->cl = &client_database[i];
                                    Writen(msg.cl_sock->sock, (void *) "PASSWORD\n" , strlen("PASSWORD\n"));
                                }
                                found = true;
                                break;
                            }
                        }
                        if(!found)
                        {
                            Writen(msg.cl_sock->sock, (void *) "Nickname not found\n" , strlen("Nickname not found\n"));
                            Writen(msg.cl_sock->sock, (void *) "NICKNAME\n" , strlen("NICKNAME\n"));
                        }
                    }
                }
                else
                {
                    Writen(msg.cl_sock->sock, (void *) "NICKNAME\n" , strlen("NICKNAME\n"));

                }
                break;

            case 0:
                //Remove \n and \r
                msg.message[len-1] = '\0';
                msg.message[len-2] = '\0';

                if(len-2 > 0)
                {
                    char c;
                    locale loc;
                    bool ok = 1;
                    for(int i=0; i<len-2; i++)
                    {
                        c = msg.message[i];
                        if(isspace(c, loc))
                        {
                            ok = 0;
                            Writen(msg.cl_sock->sock, (void *) "Please do not use whitespaces\n", strlen("Please do not use whitespaces\n"));
                            Writen(msg.cl_sock->sock, (void *) "LOGIN/REGISTER\n" , strlen("LOGIN/REGISTER\n"));
                            break;
                        }
                    }

                    if(ok)
                    {
                        if(strcmp (msg.message,"LOGIN") == 0)
                        {
                            Writen(msg.cl_sock->sock, (void *) "NICKNAME\n" , strlen("NICKNAME\n"));
                            msg.cl_sock->conn_status = 1;
                        }
                        else
                        {
                            if(strcmp (msg.message,"REGISTER") == 0)
                            {
                                Writen(msg.cl_sock->sock, (void *) "NICKNAME\n" , strlen("NICKNAME\n"));
                                msg.cl_sock->conn_status = 2;
                            }
                            else
                            {
                                Writen(msg.cl_sock->sock, (void *) "LOGIN/REGISTER\n" , strlen("LOGIN/REGISTER\n"));
                            }
                        }
                    }
                }
                else
                {
                    Writen(msg.cl_sock->sock, (void *) "LOGIN/REGISTER\n" , strlen("LOGIN/REGISTER\n"));
                }
                break;
            }

        }

        pthread_mutex_unlock(&clients_mutex);
    }
    return 0;
}
